import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    questions: [
      {
        type: 'radio',
        text: 'Выберите тип работ, для которой будете подбирать шины',
        variants: [
          {
            id: 0,
            image: require('./assets/img/image-1.jpg'),
            text: 'Сельское хозяйство'
          },
          {
            id: 1,
            image: require('./assets/img/image-2.jpg'),
            text: 'Карьер1'
          },
          {
            id: 2,
            image: require('./assets/img/image-2.jpg'),
            text: 'Карьер'
          },
          {
            id: 3,
            image: require('./assets/img/image-3.jpg'),
            text: 'Строительство'
          },
          {
            id: 4,
            image: require('./assets/img/image-3.jpg'),
            text: 'Строительство1'
          },
          {
            id: 5,
            image: require('./assets/img/image-3.jpg'),
            text: 'Строительство2'
          },
          {
            id: 6,
            image: require('./assets/img/image-3.jpg'),
            text: 'Строительство3'
          },
          {
            id: 7,
            image: require('./assets/img/image-3.jpg'),
            text: 'Строительство4'
          },
          {
            id: 8,
            image: require('./assets/img/image-3.jpg'),
            text: 'Строительство5'
          }
        ]
      },
      {
        type: 'checkbox',
        text: 'Выберите тип техники, для которой будете подбирать шины',
        variants: [
          {
            id: 0,
            image: require('./assets/img/image-1.jpg'),
            text: 'Комбайн'
          },
          {
            id: 1,
            image: require('./assets/img/image-4.jpeg'),
            text: 'Трактор малой мощности (< 220 л.с.)'
          },
          {
            id: 2,
            image: require('./assets/img/image-5.jpg'),
            text: 'Мощный трактор (> 220 л.с.)'
          }
        ]
      }
    ]
  }
})
